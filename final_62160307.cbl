       IDENTIFICATION DIVISION.
       PROGRAM-ID. FINAL-EXAM.
       AUTHOR. SAPONCHET.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader7.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader7.rpt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-REPORT-FILE.
       01  PRINT-LINE              PIC X(44).

       FD  TRADER-FILE.
       01  TRADER-RECORD.
           88 END-OF-TRADER-FILE               VALUE HIGH-VALUES.
           05 TRADER-PROVINCE      PIC XX.
           05 TRADER-USER          PIC X(4).
           05 TRADER-INCOME        PIC 9(6).

       WORKING-STORAGE SECTION.
       01  COLUMN-HEADING.
           05 FILLER               PIC X(45)
              VALUE "PROVINCE    P INCOME    MEMBER  MEMBER INCOME".
              
      *01  PAGE-FOOTING.
      *    05 PRN-MAX-PROVINCE     PIC X(14)   VALUE "MAX PROVINCE: ".
      *    05 FILLER               PIC XX      VALUE  SPACES.
      *    05 PRN-SUM-INCOME       PIC X(12)   VALUE "SUM INCOME: ".

       01  TRADER-DETAIL-LINE      OCCURS 77 TIMES.
           05 FILLER               PIC X(4)    VALUE  SPACES.
           05 PRN-TRADER-PROVINCE  PIC 99.
           05 FILLER               PIC X(5)    VALUE  SPACES.
           05 PRN-PROVINCE-INCOME  PIC $ZZZ,ZZZ,ZZ9. 
           05 FILLER               PIC X(3)    VALUE  SPACES.
           05 PRN-TOTAL-MEMBER           PIC X(6).  
           05 FILLER               PIC X(7)    VALUE  SPACES.
           05 PRN-TOTAL-MEMBER-INCOME    PIC $ZZZ,ZZZ.

       01  TRADER-TOTAL.
           05 MAX-PROVINCE         PIC XX.
           05 SUM-INCOME           PIC $ZZZ,ZZZ,ZZ9.

       01  TRADER-IDX              PIC 99.

       01  TRADER-TABLE.           
           05 TOTAL-INCOME         OCCURS 77 TIMES.
              10 P-INCOME          PIC 9(9).
              10 MEMBER-P          PIC 9(6).

       

       PROCEDURE DIVISION .
       PROCESS-TRADER-REPORT.
           OPEN INPUT TRADER-FILE 
           OPEN OUTPUT TRADER-REPORT-FILE 
           PERFORM READ-TRADER-FILE
      *    PERFORM PROCESS-MAX-PROVINCE-AND-SUM-INCOME
           PERFORM PROCESS-RESULT 

           CLOSE TRADER-FILE, TRADER-REPORT-FILE 
       .

       PROCESS-RESULT.
           DISPLAY COLUMN-HEADING 
           PERFORM VARYING TRADER-IDX FROM 1 BY 1
              UNTIL TRADER-IDX > 77
              MOVE TRADER-IDX TO PRN-TRADER-PROVINCE(77) 
              MOVE TRADER-INCOME TO PRN-TOTAL-MEMBER-INCOME(77)
           END-PERFORM
       .

      *PROCESS-MAX-PROVINCE-AND-SUM-INCOME.
      *    PERFORM UNTIL END-OF-TRADER-FILE 
      *       ADD TRADER-INCOME TO MAX-PROVINCE(TRADER-PROVINCE)
      *       ADD 1 TO 
      *.

       READ-TRADER-FILE.
           READ TRADER-FILE 
              AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ
       .